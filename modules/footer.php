<footer class="main-footer">
    <div class="float-right d-none d-sm-block">
        <b>Version</b> 1.0.0
    </div>
    <strong>Copyright &copy; 2024 <a href="#">Sistema de Biblioteca</a>.</strong> Todos los derechos reservados.
</footer>