<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Biblioteca</title>
    <link rel="stylesheet"
        href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
    <link rel="stylesheet" href="./template/plugins/fontawesome-free/css/all.min.css">
    <link rel="stylesheet" href="./template/css/adminlte.min.css">
</head>

<body class="hold-transition sidebar-mini">
    <div class="wrapper">

        <?php include_once './modules/header.php'; ?>
        <?php include_once './modules/sidebar.php'; ?>
        <?php include_once './modules/blank.php'; ?>
        <?php include_once './modules/footer.php'; ?>

    </div>

    <script src="./template/plugins/jquery/jquery.min.js"></script>
    <script src="./template/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
    <script src="./template/js/adminlte.min.js"></script>
</body>

</html>